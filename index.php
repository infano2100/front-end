<?php

$page = filter_input(INPUT_GET, 'page');
if ($page === 'logout') {
    require_once 'pages/logout.php';
}
if ($page === 'check_login') {
    require_once 'pages/check_login.php';
}

require_once 'pages/header.php'; 
if ($page == '') {
    require_once 'pages/login.php';
} else {
    require_once 'pages/'.$page.'.php';
}

require_once 'pages/footer.php';