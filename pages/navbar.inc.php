  <?php 
  // session_start();

  if (!isset($_SESSION["USER_ID"])) {
  header("Location: ".getBaseUrl());
  }  
?>  

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.min.css">
    <link rel="stylesheet" href="css/application.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- fancybox -->
    <link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" type="text/css" href="css/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <!-- fancybox -->

  <body class="layout layout-header-fixed">
    <div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="?page=user">
            <img class="navbar-brand-logo" src="img/Web-Design-Ledger-200px-tall.webp" alt="logo_web" style="height: 50px;margin-top: -15px;">
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="img/3002121059.jpg" alt="Admin">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button style="display: none;" class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4  class="navbar-text text-center"></h4>
              </li>
       
              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                  <img class="rounded" width="36" height="36" src="img/3002121059.jpg" alt="Admin" > Admin
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="?page=logout">Sign out</a></li>
                </ul>
              </li>
              <!-- <li class="visible-xs-block">
                <a href="profile.html">
                  <span class="icon icon-user icon-lg icon-fw"></span>
                  Profile
                </a>
              </li> -->
              <li class="visible-xs-block">
                <a href="?page=logout">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <div class="layout-main">
      <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">
                <li class="sidenav-search hidden-md hidden-lg">
                  <form class="sidenav-form" action="/">
                    <div class="form-group form-group-sm">
                      <div class="input-with-icon">
                        <input class="form-control" type="text" placeholder="Search…">
                        <span class="icon icon-search input-icon"></span>
                      </div>
                    </div>
                  </form>
                </li>
                <!--  <li class="sidenav-item">
                  <a href="?page=post_user">
                    <span class="sidenav-icon icon icon-keyboard-o"></span>
                    <span class="sidenav-label">Post</span>
                  </a>
                </li> -->
                <li class="sidenav-item">
                  <a href="?page=user">
                    <span class="sidenav-icon icon icon-user"></span>
                    <span class="sidenav-label">User</span>
                  </a>
                </li>
                 <li class="sidenav-item">
                  <a href="?page=logout">
                    <span class="sidenav-icon icon icon-sign-out"></span>
                    <span class="sidenav-label">Sign out</span>
                  </a>
                </li>
               <!--  <li class="sidenav-item">
                  <a href="?page=albums_user">
                    <span class="sidenav-icon icon icon-camera-retro"></span>
                    <span class="sidenav-label">Albums</span>
                  </a>
                </li> -->
                <!-- <li class="sidenav-item">
                  <a href="contacts.html">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label">Todos</span>
                  </a>
                </li> -->
              </ul>
            </nav>
          </div>
        </div>
      </div>

     

