<?php
 require "navbar.inc.php"; 
 $user_id = $_GET['user_id'];
?>

<link rel="stylesheet" href="css/profile.min.css">

<div class="layout-content">
<div class="profile-header">
            <div class="profile-cover">
              <div class="profile-container">
                <div class="profile-card">
                  <div class="profile-avetar">
                    <img class="profile-avetar-img" width="128" height="128" src="img/3002121059.jpg" alt="Teddy Wilson">
                  </div>
                  <div class="profile-overview">
                    <h1 class="profile-name"><span id="add_name"></span></h1>
                    <h4 style="color: #fff;" id="add_email"></h4>
                    <h4 style="color: #fff;" id="add_phone"></h4>
                    <h4 style="color: #fff;" id="add_website"></h4>
                    <h4 style="color: #fff;" id="add_address"></h4>
                    <h4 style="color: #fff;" id="add_company"></h4>
                  </div>
                  <div class="profile-info">
                    
                  </div>
                </div>
                <div class="profile-tabs">
                  <ul class="profile-nav">
                    <li class="active"><a href="#">Posts & Comments</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        <!-- <h1 class="text-center">Posts & Comments</h1> -->
        <div id="return_user" class="layout-content-body">
                  

        </div>
      </div>


<script type="text/javascript">

var root = 'https://jsonplaceholder.typicode.com';

$.ajax({
  url: root + '/users/<?php echo $user_id; ?>',
  method: 'GET'
}).then(function(data) {
  console.log(data);
  $("#add_name").text(data.name);
  $("#add_email").text("Email : "+data.email);
  $("#add_phone").text("phone : "+data.phone);
  $("#add_website").text("website : "+data.website);
  $("#add_address").text("address : "+data.address.street+" "+data.address.suite+" "+data.address.city+" "+data.address.zipcode);
  $("#add_company").text("company : "+data.company.name+" "+data.company.catchPhrase+" "+data.company.bs);

});


var posts = null,
    html = "";
$.ajax({
  url: root + '/posts?userId=<?php echo $user_id; ?>',
  method: 'GET'
}).then(function(data) {
  posts = data;
  $.each(data, function() {
     // html += '<div id="'+
     //   this.id +
     //   '"><h2>'+
     //   this.title + 
     //   '</h2><p>' +
     //   this.body +
     //   '</p>' +
     //   '<button class="show-comments">Show Comments</button>' +
     //   '<div class="comments"></div></div>'
     
     // ;

     html += ' <div class="post-footer">'+
                      '<h3>'+this.title+'</h3>'+
                      '<div class="post-comments">'+
                        '<div class="post-comment-list">'+
                          '<ul class="media-list">'+
                            '<li class="media">'+
                              '<div class="media-body">'+
                                '<span class="media-content">'+this.body+'</span>'+
                              '</div>'+
                            '</li>'+
                          '</ul>'+
                            '<div class="post-comment-more text-center">'+
                            
                            '<div class="comments text-left"></div>'+
                            '<span style="cursor: pointer;font-size: 18px;" class="link-muted show-comments" id_post="'+this.id+'" href="#">Show comments </span>'+
                         '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<hr>'
     
     ;
  });

  $('#return_user').html(html);
  
  $('body').on('click', '.show-comments', function() {
      var parent = $(this).parent(),
      postid = $(this).attr('id_post'),
      comments = "";
    
      console.log(postid);
      $.ajax({
        url: root + '/comments?postId='+postid,
        method: 'GET'
      }).then(function(data) {
        
        $.each(data, function() {

            comments += '<ul class="media-list">'+
                            '<li class="media">'+
                              '<a class="media-left">'+
                                '<img class="media-object" width="32" height="32" src="img/3002121059.jpg">'+
'                              </a>'+
                              '<div class="media-body">'+
                                '<span class="media-content">'+this.body+'</span>'+
                                '<br>'+
                                '<span class="media-content">'+this.email+'</span>'+
                              '</div>'+
                            '</li>'+
                          '</ul>'
        });
        parent.find('.comments').html(comments);
      });
  });
});


</script>