<?php 
require_once 'modules/config.php';

session_destroy();
unset($_SESSION['USER_ID']);

header("Location: ".getBaseUrl());

 ?>