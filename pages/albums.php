<?php
 require "navbar.inc.php"; 
 $user_id = $_GET['user_id'];

 $albums_id = "";
 if (!empty($_GET['albums_id'])) {
 $albums_id = $_GET['albums_id'];
 }

 $set_type = "";
 if (!empty($_GET['type'])) {
 $set_type = $_GET['type'];
 }
?>
<link rel="stylesheet" href="css/profile.min.css">

<div class="layout-content">
<div class="profile-header">
            <div class="profile-cover">
              <div class="profile-container">
                <div class="profile-card">
                  <div class="profile-avetar">
                    <img class="profile-avetar-img" width="128" height="128" src="img/3002121059.jpg" alt="Teddy Wilson">
                  </div>
                  <div class="profile-overview">
                    <h1 class="profile-name"><span id="add_name"></span></h1>
                    <h4 style="color: #fff;" id="add_email"></h4>
                    <h4 style="color: #fff;" id="add_phone"></h4>
                    <h4 style="color: #fff;" id="add_website"></h4>
                    <h4 style="color: #fff;" id="add_address"></h4>
                    <h4 style="color: #fff;" id="add_company"></h4>
                  </div>
                  <div class="profile-info">
                    
                  </div>
                </div>
                <div class="profile-tabs">
                  <ul class="profile-nav">
                    <!-- <li class=""><a href="#">Posts</a></li> -->
                    <?php if ($albums_id == "") { ?>
                    <li class="<?php echo  ($user_id) ? "active" : "" ; ?>"><a href="#">Albums</a></li>
                    <?php } ?>
                    <?php if ($set_type != "") { ?>
                    <li class="<?php echo  ($albums_id) ? "active" : "" ; ?>" ><a href="#">Photos</a></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div style="margin-bottom: 2em;"></div>
        <!-- <h1 class="text-center" style="margin-bottom: 2em;"> <?php echo  ($albums_id) ? "Photos" : "Albums" ; ?></h1> -->


        <?php if (!empty($albums_id)) { ?>

         <div class="col-xs-12">
              <ul class="file-list" id="return_user">
                
              </ul>
         </div>

        <?php }else{ ?>

        <div id="return_user" class="layout-content-body">


        </div>


        <?php } ?>

      </div>


<script type="text/javascript">

var root = 'https://jsonplaceholder.typicode.com';

$.ajax({
  url: root + '/users/<?php echo $user_id; ?>',
  method: 'GET'
}).then(function(data) {
  console.log(data);
  $("#add_name").text(data.name);
  $("#add_email").text("Email : "+data.email);
  $("#add_phone").text("phone : "+data.phone);
  $("#add_website").text("website : "+data.website);
  $("#add_address").text("address : "+data.address.street+" "+data.address.suite+" "+data.address.city+" "+data.address.zipcode);
  $("#add_company").text("company : "+data.company.name+" "+data.company.catchPhrase+" "+data.company.bs);

});


var type = '<?php echo $set_type;?>';


if (type != "photos") {

 $.ajax({
  url: root + '/albums?userId=<?php echo $user_id ?>',
  method: 'GET'
}).then(function(data) {
	set_val = "";
  console.log(data);
  // posts = data;
  $.each(data, function(index, val) {
     set_val += '<a target="_blank"  href="?page=albums&albums_id='+val.id+'&user_id=<?php echo $user_id ?>&type=photos"><div style="height:250px;overflow: hidden;" class="col-xs-6 col-md-3" id="'+
       val.id +
       '">'+
       '<div class="panel panel-body text-center" data-toggle="match-height" style="height: 160px;background-image: url(img/5477829604.jpg);background-size: 245px 160px;max-width: 244.75px;">'+
       '<div class="icon-with-child">'+
       // '<img  width="64" height="64" src="img/5477829604.jpg" alt="'+val.title+'">'+
       '</div>'+
       '</div>'+
       '<h5>'+
       val.title + 
       '</h5>' +
       '</div>'+
       '</a>'
     ;

  });
  $('#return_user').html(set_val);
});


}else{

$.ajax({
  url: root + '/photos?albumId=<?php echo $albums_id ?>',
  method: 'GET'
}).then(function(data) {
	set_val = "";
  console.log(data);
  // posts = data;
  $.each(data, function(index, val) {


        set_val += '<li class="file" style="height:200px;overflow: hidden;">'+
       '<a class="file-link " target="_blank" href="'+val.url+'" title="'+val.title+'">'+
       '<div class="file-thumbnail" style="background-image: url('+val.thumbnailUrl+')"></div>'+
       '<div class="file-info">' +
       '<span class="file-ext">'+val.title+'</span>'+
       '<span class="file-name"></span>'+
       '</div>' +
       '</a>' +
       '</li>' 
     ;


  });
  $('#return_user').html(set_val);
});

}



</script>