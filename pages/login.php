<div class="login">
      <div class="login-body">
        <a class="login-brand" href="/">
          <img class="img-responsive" src="img/Web-Design-Ledger-200px-tall.webp" alt="login">
        </a>
        <h3 class="login-heading">Sign in</h3>
        <div class="login-form">
        <form id="form" method="post" data-toggle="validator" action="ajax/login.php">
              <input type="hidden" name="do" value="login">

            <div class="form-group">
              <label for="username" class="control-label">Username</label>
              <input id="username" class="form-control" type="text" name="username" spellcheck="false" autocomplete="off" data-msg-required="Please enter your username." required>
            </div>
            <div class="form-group">
              <label for="password" class="control-label">Password</label>
              <input id="password" class="form-control" type="password" name="password" minlength="5" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-block" type="submit">Sign in</button>
            </div>
        <div id="login_false" class="text-center" style="padding-top: 15px;"><span class="label label-outline-danger">The user ID and password are incorrect</span></div>

          </form>
        </div>
      </div>
    </div>

    <script>
    $("#login_false").hide();

    $("#form").submit(function(e){ 


  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
  
  $.ajax(
  {
    url : formURL,
    type: "POST",
    data : postData,
    success:function(data, textStatus, jqXHR) 
    { 
      console.log(data);
      if (data == "true") {

        window.location.replace("?page=user");

      }else{

        $("#login_false").show();

      }
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
    }
  });
    e.preventDefault(); //STOP default action
});

    </script>