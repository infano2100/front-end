<?php
 require "navbar.inc.php"; 
 $user_id = $_GET['user_id'];
?>

<link rel="stylesheet" href="css/profile.min.css">

<div class="layout-content">
<div class="profile-header">
            <div class="profile-cover">
              <div class="profile-container">
                <div class="profile-card">
                  <div class="profile-avetar">
                    <img class="profile-avetar-img" width="128" height="128" src="img/3002121059.jpg" alt="Teddy Wilson">
                  </div>
                  <div class="profile-overview">
                    <h1 class="profile-name"><span id="add_name"></span></h1>
                    <h4 style="color: #fff;" id="add_email"></h4>
                    <h4 style="color: #fff;" id="add_phone"></h4>
                    <h4 style="color: #fff;" id="add_website"></h4>
                    <h4 style="color: #fff;" id="add_address"></h4>
                    <h4 style="color: #fff;" id="add_company"></h4>
                  </div>
                  <div class="profile-info">
                    
                  </div>
                </div>
                <div class="profile-tabs">
                  <ul class="profile-nav">
                    <li class="active"><a href="#">Todos List</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        
        <div class="row">
            <div class="col-xs-12">
              <div class="panel">
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-middle">
                      <thead>
                        <tr>
                          <th width="20">#</th>
                          <th class="text-center" width="200">completed</th>
                          <th width="200">title</th>
                        </tr>
                      </thead>
                      <tbody id="return_todos">

                        

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>


<script type="text/javascript">

var root = 'https://jsonplaceholder.typicode.com';

$.ajax({
  url: root + '/users/<?php echo $user_id; ?>',
  method: 'GET'
}).then(function(data) {
  // console.log(data);
  $("#add_name").text(data.name);
  $("#add_email").text("Email : "+data.email);
  $("#add_phone").text("phone : "+data.phone);
  $("#add_website").text("website : "+data.website);
  $("#add_address").text("address : "+data.address.street+" "+data.address.suite+" "+data.address.city+" "+data.address.zipcode);
  $("#add_company").text("company : "+data.company.name+" "+data.company.catchPhrase+" "+data.company.bs);

});


var posts = null,
    html = "";
$.ajax({
  url: root + '/todos?userId=<?php echo $user_id; ?>',
  method: 'GET'
}).then(function(data) {

  var i = 1;
  $.each(data, function(index, val) {

    var check_box = "";
    if (val.completed == true) {
      var check_box = '<input class="custom-control-input" type="checkbox" checked >';
    }else{
      var check_box = '<input class="custom-control-input" type="checkbox">';
    }
              html +=    '<tr>'+
                         '<td>'+i+'</td>'+
                         '<td class="text-center">'+
                         '<label class="custom-control custom-control-primary custom-checkbox">'+
                         check_box+
                         '<span class="custom-control-indicator"></span>'+
                         '</label>'+
                         '</td>'+
                         '<td class="nowrap">'+
                         '<strong>'+val.title+'</strong>'+
                         '</td>'+
                         '</tr>'
     ;

    i++;
  });

  $('#return_todos').html(html);
  
});


</script>